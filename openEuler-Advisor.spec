Name:       openEuler-Advisor
Version:    1.0.1
Release:    2
Summary:    Collection of automatic tools for easily maintaining openEuler 
Group:	    Application
License:    Mulan PSL v2
URL:	    https://gitee.com/openeuler/openEuler-Advisor
Source0:    https://gitee.com/openeuler/%{name}/repository/archive/v%{version}.tar.gz
Patch0:     backport-0001-adapt-to-the-code-structure-of-openeuler-community.patch
BuildArch:  noarch
BuildRequires: python3-pytest
Requires:   python3-pyrpm python3-pyyaml python3-requests rpmdevtools python3-beautifulsoup4 dnf-plugins-core libabigail
Requires:   git

%description
Collection of automatic tools for easily maintaining openEuler

%prep
%autosetup -n %{name}-v%{version} -p1

%build
%py3_build

%install
%py3_install

%check
py.test-%{python3_version} -vv tests || :

%post

%postun

%files
%doc README.* AUTHORS RELEASES.md
%license LICENSE 
%{python3_sitelib}/*
%attr(0755,root,root) %{_bindir}/simple_update_robot
%attr(0755,root,root) %{_bindir}/oa_upgradable
%attr(0755,root,root) %{_bindir}/check_missing_file
%attr(0755,root,root) %{_bindir}/check_repeated_repo
%attr(0755,root,root) %{_bindir}/check_version
%attr(0755,root,root) %{_bindir}/check_source_url
%attr(0755,root,root) %{_bindir}/create_repo
%attr(0755,root,root) %{_bindir}/create_repo_with_srpm
%attr(0755,root,root) %{_bindir}/psrtool
%attr(0755,root,root) %{_bindir}/review_tool
%attr(0755,root,root) %{_bindir}/tc_reminder
%attr(0755,root,root) %{_bindir}/which_archived
%attr(0755,root,root) %{_bindir}/prow_review_tool

%changelog
* Wed Jan 13 2022 liuqi469227928 <469227928@qq.com> - 1.0.1-2
- adpat to the code structure of openeuler/community

* Wed Sep 22 2021 licihua <licihua@huawei.com> - 1.0.1-1
- Update to v1.0.1 

* Tue Sep 14 2021 liuqi469227928 <469227928@qq.com> - 1.0.0-15
- reviewer_checklist: change script excute path of sanity_check.py

* Fri Aug 27 2021 liuqi469227928 <469227928@qq.com> - 1.0.0-14
- review_tool: change text of note

* Sun Jun 20 2021 licihua <licihua@huawei.com> - 1.0.0-13
- review_tool: support delete sig

* Wed Feb 24 2021 shanshishi <shanshishi@huawei.com> - 1.0.0-12
- review_tool: support continuous items fast set

* Sat Feb 20 2021 shanshishi <shanshishi@huawei.com> - 1.0.0-11
- review_tool: add command for retrigger rebuilding review list
- review_tool: review_tool:fix PR conflicts
- review_tool: do some improvements

* Sun Feb 7 2021 shanshishi <shanshishi@huawei.com> - 1.0.0-10
- prow: rebuild review list if source branch updated

* Sun Feb 7 2021 shanshishi <shanshishi@huawei.com> - 1.0.0-9
- review tool: support file name contain whitespace characters
- review tool: add require 'git'

* Wed Feb 3 2021 shanshishi <shanshishi@huawei.com> - 1.0.0-8
- fix get_issues() and get_issue_comments() can not run correctly

* Wed Jan 27 2021 licihua <licihua@huawei.com> - 1.0.0-7
- fix s.chdir(u_pkg) return exception

* Wed Jan 27 2021 licihua <licihua@huawei.com> - 1.0.0-6
- fix setup.py bug

* Mon Jan 25 2021 licihua <licihua@huawei.com> - 1.0.0-5
- support openEuler 20.9

* Fri Jan 22 2021 licihua <licihua@huawei.com> - 1.0-4
- check_abi: add miss require "libabigail"

* Tue Dec 1 2020 smileknife <jackshan2010@aliyun.com> - 1.0-3
- review_tool: support for ci/cd framework prow

* Tue Dec 1 2020 smileknife <jackshan2010@aliyun.com> - 1.0-2
- Optimize editing mode for review items

* Sat Oct 17 2020 Leo Fang <leofang_94@163.com> - 1.0-1
- Package init
